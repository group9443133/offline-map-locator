import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import { VitePWA } from 'vite-plugin-pwa'


export default defineConfig({
  plugins: [react() ,     VitePWA({
    registerType: 'autoUpdate',
    injectRegister:'inline',
    devOptions: {
      enabled: true
    },
    workbox: {
      globPatterns: ['**/*.{js,css,html,ico,png,svg}'],
      runtimeCaching: [
        {
          urlPattern: /^https:\/\/api\.neshan\.org\/v1\/search/,
          handler: 'CacheFirst',
          options: {
            cacheName: 'api-cache',
          },
        },
      ],
    }
  })],


})
