import React, { createContext, useContext, useRef } from 'react';

const MapContext = createContext();

export const MapProvider = ({ children }) => {
  const mapRef = useRef(null);
  const markerRef = useRef(null);


  return (
    <MapContext.Provider value={{ mapRef , markerRef }}>
      {children}
    </MapContext.Provider>
  );
};

export const useMapContext = () => {
  return useContext(MapContext);
};
