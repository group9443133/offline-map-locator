import { useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
import { useMapContext } from '../../context/MapContext';

const Marker = ({ lng, lat }) => {
  const { mapRef , markerRef } = useMapContext();

  useEffect(() => {
    if (lng && lat ) {

      markerRef.current = new mapboxgl.Marker({ color: 'blue', draggable: true })
      .setLngLat([lng, lat])
      .addTo(mapRef.current);
      mapRef.current.setCenter([lng,lat]);
      mapRef.current.setZoom(15);

      return () => {
        markerRef.current.remove();
      };
    }
  }, [lng, lat]);

  return null;
};

export default Marker;
