import React, { useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
import './index.css';
import { useMapContext } from '../../context/MapContext';

const Map = () => {
  const { mapRef } = useMapContext();

  useEffect(() => {
    let ltt = 57;
    let lng = 37;

    mapboxgl.accessToken =
      'pk.eyJ1IjoicmV5aGFuZS1ic3RwIiwiYSI6ImNsbDBweW1yMDAwbHYzZW9kd3p3aGF5MW0ifQ.Y5Lto5zl1CL1RfO-Z1NoFw';

    if (!mapRef.current) {
      mapRef.current = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [lng, ltt],
        zoom: 9,
      });

      // Wait for the map to load before setting the center based on geolocation
      mapRef.current.on('load', () => {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function (position) {
            ltt = position.coords.latitude;
            lng = position.coords.longitude;
            mapRef.current.setCenter([lng, ltt]);
          });
        }
      });
    }

    return () => {
      if (mapRef.current) {
        mapRef.current.remove();
        mapRef.current = null;
      }
    };
  }, []);

  return <div className="map" id="map" />;
};

export default Map;
