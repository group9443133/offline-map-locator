import { useEffect, useState, useRef } from 'react';
import './index.css';
import Marker from '../marker/Marker';

const Searchbar = () => {

    const [input, setInput] = useState('');
    const [result, setResult] = useState([]);
    const [selectedLocation, setSelectedLocation] = useState(null);
    const [showResults, setShowResults] = useState(false)
    const searchResultRef = useRef(null);
    const [lng, setlng] = useState('');
    const [lat, setlat] = useState('');


  useEffect(() => {
    const handleDocumentClick = (event) => {
      if (searchResultRef.current && !searchResultRef.current.contains(event.target)) {
        setShowResults(false);
      }
    };

    document.addEventListener('mousedown', handleDocumentClick);

    return () => {
      document.removeEventListener('mousedown', handleDocumentClick);
    };
  }, []);

  useEffect(() => {

    let longitude = 36.313287 ;
    let latitude = 59.578749 ;

    

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        longitude =  (position.coords.latitude);
        latitude =  ( position.coords.longitude);
      });
    }


    fetch(`https://api.neshan.org/v1/search?term=${input}&lat=${latitude}&lng=${longitude}`, {
        method: "GET",
        headers: { "Api-Key": "service.20e9723c75914e439174be6933263afe" },
        })
      .then(async (resp) => {
        let data;
        const cache = await caches.open('api-cache');
        const cachedResponse = await cache.match(resp.url);

        if (cachedResponse) {
          data = await cachedResponse.json();
        } else {
          data = await resp.json();
          cache.put(resp.url, new Response(JSON.stringify(data)));
        }

        setResult(data);
      })
      .catch((error) => {
        console.log(error);
      });

  }, [input]);


  


  return (
    <>
      <input dir='rtl'
        className="searchbar"
        type="text"
        placeholder='جست و جو '
        value={input}
        onChange={(e) => {setInput(e.target.value); setShowResults(true);}}
      />
      {showResults && (
        <ul dir='rtl' className='search-result' ref={searchResultRef}>
          {result.items?.map((item, index) => (
            <li key={index} onClick={() => {setlng(item?.location?.x) ; setlat(item?.location?.y); setInput(item.title); setShowResults(false);}}>{item.title}</li>
          ))}
        </ul>
      )}
      
    <Marker lng={lng} lat={lat} /> 
 
    </>
  );
}

export default Searchbar;
