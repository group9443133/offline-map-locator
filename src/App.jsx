import Map from "./components/map/Map"
import Searchbar from "./components/search/Searchbar"
import InstallPWAButton from "./components/installBtn/InstallPWAButton"




function App() {
 

  return (
    <>
      {/* <InstallPWAButton/> */}
      <Searchbar />
      <Map />
      
    </>
  )
}

export default App
