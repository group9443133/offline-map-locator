# React Location Search App with Vite and Yarn

**Description:** This is a React application that allows users to search for locations on a map using the Neshan Search API. It also includes caching of search results for improved performance.

## Table of Contents

- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)
- [Deployment](#deployment)
- [Built With](#built-with)
- [Contributing](#contributing)

## Getting Started

### Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js: Make sure you have Node.js installed. You can download it from [nodejs.org](https://nodejs.org/).

### Installation

To get started with this project, follow these steps:

1. Clone the repository:

shell git clone https://gitlab.com/group9443133/offline-map-locator


2. Change to the project's directory:


-cd Offline-Map-Locator


3. Install project dependencies using Yarn:



-yarn install


### Usage

To run the development server and start using the app, use the following command:



-yarn dev


This will start the development server, and you can access the app in your browser at http://localhost:3000

### Configuration

To configure the app, you'll need to set up your Neshan API key. Follow these steps:

1. Obtain a Neshan API key from Neshan Developer Portal
2. Create a .env file in the project's root directory and add your API key like this:



-REACT_APP_NESHAN_API_KEY=your-api-key-here


### Deployment

To deploy the app for production, you can use the following command:




### Deployment

To deploy the app for production, you can use the following command:



-yarn build


This will create an optimized build of your application in the dist directory, which you can then deploy to your hosting platform of choice.

### Built With 

- React - The JavaScript library used for building the user interface.
- Vite - The build tool and development server.
- Yarn - The package manager for managing project dependencies.
- Neshan Search API - The API used for location search and caching.

### Contributing 

We welcome contributions to improve this project. To contribute:

1. Fork the repository on GitLab.
2. Create a new branch with your changes: `git checkout -b feature/your-feature`.
3. Commit your changes: `git commit -m 'Add some feature'`.
4. Push to the branch: `git push origin feature/your-feature`.
5. Create a merge request on GitLab.

